@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <div class="col mx-auto">
                            <img style="width: 100%" src="/storage/cover_images/{{$posts->cover_image}}">
                            <br><br>
                            <div class="text-center">
                                <h3>{{$posts->title}}</h3>
                                <h5>{{$posts->body}}</h5>
                                <small><i>Created by: {{auth()->user()->name}} at {{$posts->created_at}}</i></small>
                            </div>
                            @if(auth()->user()->id == $posts->user_id)
                                <div class="row pull-right">
                                    <a class="btn btn-primary" href="/posts/{{$posts->id}}/edit">Edit</a>
                                    {!! Form::open(['action'=> ['PostsController@destroy', $posts->id], 'method' => 'POST']) !!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
                                    {!! Form::close() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection