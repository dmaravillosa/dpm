@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if(count($posts) > 0)
                @foreach($posts as $post)
                    <div class="card">
                        {{--<div class="card-header">{{$post->title}}</div>--}}

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <a style="text-decoration: none; color: black;" href="/posts/{{$post->id}}">
                                <div class="row h-100">
                                    <div class="col-md-4 col-sm-4">
                                        <img style="width: 100%;" src="/storage/cover_images/{{$post->cover_image}}">
                                    </div>
                                    <div class="col-md-4 col-sm-4 my-auto">
                                        <h3>{{$post->title}}</h3>
                                        <h5>{{$post->body}}</h5>
                                        <small>Created by: {{$post->user_id }} at {{$post->created_at }}</small>
                                    </div>

                                </div>
                            </a>
                        </div>
                    </div>
                    <br>
                @endforeach
                {{$posts->links()}}
            @else
                There are no post to display.
            @endif


        </div>
    </div>
</div>
@endsection
