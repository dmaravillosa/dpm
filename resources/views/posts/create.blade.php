@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @if($method == 'create')
                    <div class="card-header">Create Post</div>

                    <div class="card-body">
                        {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            <div class="form-group">
                                {!! Form::label('title', 'Title') !!}
                                {!! Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('body', 'Description') !!}
                                {!! Form::textarea('body', '', ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::file('cover_image') !!}
                            </div>

                        <a class="btn btn-danger" href="/posts">Back</a>
                        {!! Form::submit('Submit', ['class' => 'btn btn-primary float-right']) !!}
                        {!! Form::close() !!}
                    </div>

                    @else
                        <div class="card-header">Edit Post</div>

                        <div class="card-body">
                            {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            <div class="form-group">
                                {!! Form::label('title', 'Title') !!}
                                {!! Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('body', 'Description') !!}
                                {!! Form::textarea('body', $post->body, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            </div>
                            <div class="form-group col-md-3 col-sm-3">
                                {!! Form::file('cover_image', ['src' => $post->cover_image]) !!}
                            </div>

                            <a class="btn btn-danger" href="/posts">Back</a>
                            {!! Form::hidden('_method', 'PUT') !!}
                            {!! Form::submit('Submit', ['class' => 'btn btn-primary float-right']) !!}
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
