<?php
/**
 * Created by: Daniel Maravillosa
 * Created at: 2018 - 4 - 6
 *
 *
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $posts = Posts::orderBy('created_at', 'desc')->paginate(5);
        return view('posts.home')->with('posts', $posts);
    }
}
